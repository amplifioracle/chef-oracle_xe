oracle_xe Cookbook
========================
This cookbook installs Oracle XE Database

Attributes
----------

### oracle_xe::default

**\['oracle_xe'\]\['user'\]** (String)
The user you want to grant dba access. root automatically added. (*No Default*)

**\['oracle_xe'\]\['url'\]** (String)
The URL for the RPM installer file (*No Default*)

**\['oracle_xe'\]\['checksum'\]** (String)
The SHA256 hash of the installer file (*No Default*)

**\['oracle_xe'\]\['version'\]** (String)
The version of XE being installed (*No Default*)

**\['oracle_xe'\]\['http_port'\]** (Integer)
The port for the HTTP listener (*Default*: 8080)

**\['oracle_xe'\]\['listener_port'\]** (Integer)
The port for the DB listener (*Default*: 1521)

**\['oracle_xe'\]\['system_password'\]** (String)
The SYS and SYSTEM password (*Default*: admin)

**\['oracle_xe'\]\['start_on_boot'\]** (Boolean)
Start the database on system boot (*Default*: true)



Usage
-----
#### oracle_xe

Just include `oracle_xe` in your node's `run_list`, changing attributes above where applicable:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[oracle_xe]"
  ]
}
```

License and Authors
-------------------
Authors: Brian Celenza <brian.celenza@amplificommerce.com>
