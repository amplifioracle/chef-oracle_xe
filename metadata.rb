name             'oracle_xe'
maintainer       'Amplifi Commerce'
maintainer_email 'brian.celenza@amplificommerce.com'
license          'All rights reserved'
description      'Installs/Configures oracle_xe'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
