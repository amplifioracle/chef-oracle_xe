default['oracle_xe']['user'] = ''
default['oracle_xe']['url'] = ''
default['oracle_xe']['checksum'] = ''
default['oracle_xe']['version'] = ''

default['oracle_xe']['http_port'] = 8080
default['oracle_xe']['listener_port'] = 1521
default['oracle_xe']['system_password'] = 'admin'
default['oracle_xe']['start_on_boot'] = true