#
# Cookbook Name:: oracle_xe
# Recipe:: default
#
# Copyright 2014, Amplifi Commerce
#
# All rights reserved - Do Not Redistribute
#
src_filename = node['oracle_xe']['url'].split('/').last
src_filepath = "#{Chef::Config[:file_cache_path]}/#{src_filename}"

# download the installer
remote_file src_filename do
  path src_filepath
  source node['oracle_xe']['url']
  checksum node['oracle_xe']['checksum']
  action :create_if_missing
  retries 3
end

# install the silent template
silent_properties_filepath = "#{Chef::Config[:file_cache_path]}/oracle_xe.rsp"
template silent_properties_filepath do
  source "oracle_xe.rsp.erb"
  action :create
end

# ubuntu needs some love (aka hackery)
if node["platform"] == "ubuntu"
  package 'libaio1'
  package 'unixodbc'
end

if node["platform"] == "ubuntu"
  chkconfig_path = "/sbin/chkconfig"
  template chkconfig_path do
    source "chkconfig.erb"
    action :create
    mode 0755
  end

  oracle_conf_path = "/etc/sysctl.d/60-oracle.conf"
  template oracle_conf_path do
    source "60-oracle.conf.erb"
    action :create
  end

  bash 'tuning_os_params' do
    code <<-EOH
      service procps start
      ln -s /usr/bin/awk /bin/awk
      mkdir /var/lock/subsys
      touch /var/lock/subsys/listener
    EOH
  end

  bash 'setting_swap' do
    code <<-EOH
      dd if=/dev/zero of=/swapfile bs=1024 count=2097152
      mkswap /swapfile
      swapon /swapfile
      cp /etc/fstab /etc/fstab.orig
      echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
    EOH
  end
end

# install xe package
bash 'install_xe' do
  if node["platform"] == "ubuntu"
    code "dpkg --install #{src_filepath}"
  else
    code "rpm -ivh #{src_filepath}"
  end
end



# configure w/ response file
bash 'configure_xe' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    sleep 10
    /etc/init.d/oracle-xe configure responseFile=#{silent_properties_filepath}
    EOH
end

# add env vars to profile
ini_path = "/u01/app/oracle/product/#{node['oracle_xe']['version']}/xe/bin/oracle_env.sh"
bash 'oracle_xe_env_vars' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    cp #{ini_path} /etc/profile.d/oracle_env.sh
    source #{ini_path}
    EOH
end

bash 'oracle_xe_root_dba' do
  code <<-EOH
    usermod -aG dba root
    EOH
end

# if another user was specified, grant it access
bash "oracle_xe_#{node['oracle_xe']['user']}_dba" do
  code <<-EOH
    usermod -aG dba #{node['oracle_xe']['user']}
    EOH
  not_if { node['oracle_xe']['user'].empty? }
end
